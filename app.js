new Vue({
  el: "#app",
  data: {
    event: {
      eventDate: " June 9th - 16th",
      eventTitle: "Summer Festival!",
      signUpText: "Add your name to the guest list for <em>exclusive</em> offers:",
      eventDescription:
        "It's back! This years summer Coding festival will be in the beautiful town of Hauppauge!",
    },
    newNameText: "",
    guestName: [],
    appStyles: {
      marginTop: "25px",
    },
    eventCapacity: 25,
    eventCapacityPercentage: 0
  },
  methods: {
    formSubmitted: function () {
      if (this.newNameText.length > 0 && this.eventCapacityPercentage < 100) {
        this.guestName.push(this.newNameText);
        this.newNameText = ""
        this.eventCapacityPercentage = this.guestName.length / (this.eventCapacity / 100)
        
      }
    }
  },
   computed: {
       sortNames: function () {
         
           return this.guestName.sort()
       }
   },
   filters: {
       toUpper: function (value) {
           return value.toUpperCase()
       }
   }
});